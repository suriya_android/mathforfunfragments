package com.suriya.mathforfunfragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.suriya.mathforfunfragments.databinding.FragmentTitleBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding:FragmentTitleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private var getCorrect:Int = 0
    private var getIncorrect:Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_title,container,false)
        // Do
        setHasOptionsMenu(true)
        val args = TitleFragmentArgs.fromBundle(requireArguments())
        getCorrect = args.gloCorrect
        getIncorrect = args.gloIncorrect
        Log.e("Test Value",getCorrect.toString() +"::"+getIncorrect.toString())
        Update()
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,requireView().findNavController()) || super.onOptionsItemSelected(item)
    }

    private fun Update(){
        binding.apply {
            txtGlobalCor.text = "ถูก : $getCorrect"
            txtGlobalIncor.text = "ไม่ถูก : $getIncorrect"
            btnplusGame.setOnClickListener {
                val action = TitleFragmentDirections.actionTitleFragment2ToPlusGameFragment()
                action.plusCorrect = getCorrect
                action.plusIncorrect = getIncorrect
                it.findNavController().navigate(action)
            }
            btnminusGame.setOnClickListener {
                val action = TitleFragmentDirections.actionTitleFragment2ToMinusGameFragment()
                action.minusCorrect = getCorrect
                action.minusIncorrect = getIncorrect
                it.findNavController().navigate(action)
            }
            btnmulGame.setOnClickListener {
                val action = TitleFragmentDirections.actionTitleFragment2ToMultipleGameFragment()
                action.mulCorrect = getCorrect
                action.mulIncorrect = getIncorrect
                it.findNavController().navigate(action)
            }
        }
    }

//    requireActivity().onBackPressedDispatcher.addCallback(this){
//        val action =
//            PlusGameFragmentDirections.actionPlusGameFragmentToTitleFragment2()
//        action.gloCorrect = correct
//        action.gloIncorrect = incorrect
//        Log.e("Test",action.gloCorrect.toString())
//        view?.findNavController()?.navigate(action)

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TitleFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TitleFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}