package com.suriya.mathforfunfragments


import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.suriya.mathforfunfragments.databinding.FragmentPlusGameBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusGameFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    //    Declare
    private var num1 = 0
    private var num2 = 0
    private var sum = 0
    private var result = ""
    private var correct = 0
    private var incorrect = 0
    private var i = 0
    private var btnValue1 = 0
    private var btnValue2 = 0
    private var btnValue3 = 0
    private var getCorrect:Int = 0
    private var getInCorrect:Int = 0
    private lateinit var binding: FragmentPlusGameBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_plus_game,
            container,
            false
        )
        onBackPressed()
        val args = PlusGameFragmentArgs.fromBundle(requireArguments())
        getCorrect = args.plusCorrect
        getInCorrect = args.plusIncorrect
        Update()
        return binding.root
    }

    private fun onBackPressed(){
        requireActivity().onBackPressedDispatcher.addCallback(this){
            val action = PlusGameFragmentDirections.actionPlusGameFragmentToTitleFragment2()
            action.gloCorrect = correct + getCorrect
            action.gloIncorrect = incorrect + getInCorrect
            Log.e("Test",action.gloCorrect.toString())
            view?.findNavController()?.navigate(action)
        }
    }

    private fun Update() {
        randomQuestion()
        initialButtonAnswer()
        binding.apply {
            checkAnswer(btnAns1Plus)
            checkAnswer(btnAns2Plus)
            checkAnswer(btnAns3Plus)
        }
        initialToView()
    }

    private fun randomQuestion() {
        num1 = Random.nextInt(1, 5)
        num2 = Random.nextInt(1, 5)
        sum = num1 + num2
    }

    private fun initialToView() {
        binding.apply {
            txtNum1Plus.text = num1.toString()
            txtNum2Plus.text = num2.toString()
            txtResultPlus.text = ""
            btnAns1Plus.text = btnValue1.toString()
            btnAns2Plus.text = btnValue2.toString()
            btnAns3Plus.text = btnValue3.toString()
            txtCorrectPlus.text = ": $correct"
            txtIncorrectPlus.text = ": $incorrect"
            txtResultPlus.text = result
        }
    }

    private fun initialButtonAnswer() {
        i = Random.nextInt(1, 3)
        when (i) {
            1 -> {
                btnValue1 = sum
                btnValue2 = sum + 1
                btnValue3 = sum + 2
            }
            2 -> {
                btnValue2 = sum
                btnValue1 = sum - 1
                btnValue3 = sum + 1
            }
            3 -> {
                btnValue3 = sum
                btnValue1 = sum - 2
                btnValue2 = sum - 1
            }
        }
    }

    private fun checkAnswer(btn:Button){
        btn.setOnClickListener {
            if(btn.text == sum.toString()){
                result = "ถูกต้อง"
                correct++
            }else{
                result = "ไม่ถูกต้อง"
                incorrect++
            }
            Handler().postDelayed({
                Update()
            },250)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PlusGameFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PlusGameFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}